# Student Expectations

### Student: *David Fuhry*

I don't really know what to expect so I'm going with:
* Some coding in R
* Working on a small project
* Perhaps some more funny comic strips like [this one](https://xkcd.com/323/)
* Hopefully: **No Java**
